<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Product;
use App\ProductsAttribute;
use App\ProductsImage;
use DB;
use App\Order;
use App\Category;

class InstamozoController extends Controller
{
    public function pay(Request $request){

        $api = new \Instamojo\Instamojo(
               config('services.instamojo.api_key'),
               config('services.instamojo.auth_token'),
               config('services.instamojo.url')
           );


           $order_id = Session::get('order_id');
           $grand_total = Session::get('grand_total');
           $orderDetails = Order::getOrderDetails($order_id);
           $orderDetails = json_decode(json_encode($orderDetails));
           /*echo "<pre>"; print_r($orderDetails); die;*/
           $nameArr = explode(' ',$orderDetails->name);
           $purpose = "ATS Store";

       try {
           $response = $api->paymentRequestCreate(array(
               "purpose" => $purpose,
               "amount" => $grand_total,
               "buyer_name" => $nameArr[0],
               "send_email" => true,
               "email" => $orderDetails->user_email,
               "phone" => $orderDetails->mobile,
               "redirect_url" => "http://localhost:1000/instamozo/thanks"
               ));

               header('Location: ' . $response['longurl']);
               exit();
       }catch (Exception $e) {
           print('Error: ' . $e->getMessage());
       }
       DB::table('cart')->where('user_email',$orderDetails->user_email)->delete();

    }


    public function instamozoThanks(){
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        
        return view('orders.thanks_instamozo',compact('categories'));

    }

    public function success(Request $request){
        try {

           $api = new \Instamojo\Instamojo(
               config('services.instamojo.api_key'),
               config('services.instamojo.auth_token'),
               config('services.instamojo.url')
           );

           $response = $api->paymentRequestStatus(request('payment_request_id'));

           if( !isset($response['payments'][0]['status']) ) {
              dd('payment failed');
           } else if($response['payments'][0]['status'] != 'Credit') {

                dd('payment failed');
           }
         }catch (\Exception $e) {
            dd('payment failed');
        }
       dd($response);
     }
     

}
