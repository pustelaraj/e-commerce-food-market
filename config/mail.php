<?php  
return [
    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'mail.dotbotics.in'),
    'port' => env('MAIL_PORT', 26),
    'from' => [
        'address' => env('MAIL_FROM_ADDRESS', 'test@dotbotics.in'),
        'name' => env('MAIL_FROM_NAME', 'E-Commerce Website'),
    ],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    //'username' => env('raja.mtmc@gmail.com'),
    //'password' => env('jyothiraj123@'),
    'username' => 'test@dotbotics.in',
    'password' => 'Ats@2020',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => [
        'theme' => 'default',
        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],
    'stream' => [
        'ssl' => [
           'allow_self_signed' => true,
           'verify_peer' => false,
           'verify_peer_name' => false,
        ],
     ],

];
