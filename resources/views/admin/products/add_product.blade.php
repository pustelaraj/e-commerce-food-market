@extends('layouts.adminLayout.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb">
     <h5>Products</h5> 
     <div class="bc_div"> 
      <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
      <a href="#">Products</a> <a href="#" class="current">Add Product</a>
     </div> 
   </div>
<!--
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Products</a> <a href="#" class="current">Add Product</a> </div>
    <h1>Products</h1>
-->
    @if(Session::has('flash_message_error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_error') !!}</strong>
            </div>
        @endif   
        @if(Session::has('flash_message_success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                    <strong>{!! session('flash_message_success') !!}</strong>
            </div>
        @endif
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Product</h5>
          </div>
          <div class="widget-content">
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('admin/add-product') }}" name="add_product" id="add_product" novalidate="novalidate">{{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Under Category</label>
                <div class="controls">
                  <select name="category_id" id="category_id" style="width:220px;">
                    <?php echo $categories_drop_down; ?>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Product Name</label>
                <div class="controls">
                  <input type="text" name="product_name" id="product_name">
                </div>
              </div>
              <!--<div class="control-group">
                <label class="control-label">Product Code</label>
                <div class="controls">
                  <input type="text" name="product_code" id="product_code">
                </div>
              </div>-->
              <div class="control-group">
                <label class="control-label">Product Brand</label>
                <div class="controls">
                  <input type="text" name="product_brand" id="product_color">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                  <textarea name="description"  id="textarea_editor" class="textarea_editor span12"></textarea>
                </div>
              </div>
              <!--
                <div class="control-group">
                <label class="control-label">Material & Care</label>
                <div class="controls">
                  <textarea name="care" class="textarea_care span12"></textarea>
                </div>
              </div>
              -->
              <div class="control-group">
                <label class="control-label">Food Preference</label>
                <div class="controls">
                  <select name="sleeve" class="form-control">
                    <option value="">Select Food Preference</option>
                    @foreach($foodpreference as $foodpreference)
                    <option value="{{ $foodpreference }}">{{ $foodpreference }}</option>                      
                    @endforeach
                   
                  </select>  
                </div>
              </div>
              <!--<div class="control-group">
                <label class="control-label">Pattern</label>
                <div class="controls">
                  <select name="pattern" class="form-control">
                    <option value="">Select Pattern</option>
                    @foreach($patternArray as $pattern)
                      <option value="{{ $pattern }}">{{ $pattern }}</option>
                    @endforeach
                  </select>  
                </div>
              </div>-->
              <div class="control-group">
                <label class="control-label">Actual Price</label>
                <div class="controls">
                  <input type="text" name="actual_price" id="actual_price">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Discount</label>
                <div class="controls">
                  <input type="text" name="discount" id="discount" onkeyup="getPrice()">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Price</label>
                <div class="controls">
                  <input type="text" name="price" id="price">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Weight (g)</label>
                <div class="controls">
                  <input type="text" name="weight" id="weight">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Image</label>
                <div class="controls">
                  <div id="uniform-undefined"><input name="image" id="image" type="file"></div>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Video</label>
                <div class="controls">
                  <div id="uniform-undefined"><input name="video" id="video" type="file"></div>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Feature Item</label>
                <div class="controls">
                  <input type="checkbox" name="feature_item" id="feature_item" value="1">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Enable</label>
                <div class="controls">
                  <input type="checkbox" name="status" id="status" value="1">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Deal Product</label>
                <div class="controls">
                  <input type="checkbox" name="deal_status" id="deal_status" value="1">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Deal Expiry Date</label>
                <div class="controls">
                  <input type="datetime-local" name="deals_expiry_date" id="deals_expiry_date">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Special Product Status</label>
                <div class="controls">
                  <input type="checkbox" name="special_status" id="special_status" value="1">
                </div>
              </div>






              <div class="form-actions">
                <input type="submit" value="Add Product" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection