@extends('layouts.frontLayout.front_design')

@section('content')

<section id="slider"><!--slider-->
		
</section><!--/slider-->
	
<section id="listing">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3">
				@include('layouts.frontLayout.front_sidebar')
			</div>
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">
						@if(!empty($search_product))
							{{ $search_product }} Item
						@else
							{{ $categoryDetails->name }} Items
						@endif
						    ({{ count($productsAll) }})
					</h2>
					<div align="left"><?php echo $breadcrumb; ?></div>
					<div>&nbsp;</div>
					@foreach($productsAll as $pro)
				<div class="col-md-3 col-sm-6">
         <div class="product-grid2">
          <div class="product-image2">
           <a href="{{ url('/product/'.$pro->id) }}">
            <img class="pic-1" src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}">
			<img class="pic-2" src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}">
<!--            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo3/images/img-2.jpeg">-->
           </a>
           <ul class="social">
<!--            <li><a href="{{ url('/product/'.$pro->id) }}" data-tip="Quick View"><i class="fa fa-eye"></i></a></li>-->
            <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-star"></i></a></li>
            <li><a href="{{ url('/product/'.$pro->id) }}" data-tip="View Detailst"><i class="fa fa-shopping-cart"></i></a></li>
           </ul>
           <a class="add-to-cart" href="#">View Details</a>
          </div>
          <div class="product-content">
           <h3 class="title"><a href="{{ url('/product/'.$pro->id) }}">{{ $pro->product_name }}</a></h3>
           <div class="cost">INR <span class="price">{{ $pro->price }}</span></div>
          </div>
         </div>
        </div>
		
					@endforeach
					@if(empty($search_product))
						<div align="center">{{ $productsAll->links() }}</div>
					@endif
				</div>
				
			</div>
		</div>
	</div>
</section>

@endsection