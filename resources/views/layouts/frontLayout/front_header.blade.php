<?php 
use App\Http\Controllers\Controller;
use App\Product;
$mainCategories =  Controller::mainCategories();
$cartCount = Product::cartCount();
?>
<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#">Welcome To Our Fruit’s Online Store</a></li>
<!--								<li><a href="#"><i class="fa fa-envelope"></i> info@stack-developers.xyz</a></li>-->
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="pull-right">
							<ul class="nav navbar-nav">
					<li><a href="{{ url('/wish-list') }}">Wishlist</a></li>
								<li><a href="{{ url('/orders') }}"> Orders</a></li>
<!--								<li><a href="{{ url('/cart') }}">Cart ({{ $cartCount }})</a></li>-->
								@if(empty(Auth::check()))
									<li><a href="{{ url('/login-register') }}"> Login</a></li>
								@else
									<li><a href="{{ url('/account') }}"> Account</a></li>
									<li><a href="{{ url('/user-logout') }}"> Logout</a></li>
								@endif
<!--
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
-->
							</ul>
						</div>
                     
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container-fluid">
				<div class="row">
                 <div class="col-sm-4">
                  	<div class="search_box">
							<form action="{{ url('/search-products') }}" method="post">{{ csrf_field() }} 
								<input type="text" placeholder="Search Product" name="product" />
								<button type="submit" style="border:0px; height:35px; margin-left:-3px">Go</button>
							</form>
						</div>
                 </div>
					<div class="col-sm-4">
						<div class="logo">
							<a href="{{ url('./')}}"><img src="{{ asset('images/frontend_images/home/logo.png') }}" alt="" /></a>
						</div>
<!--
						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canada</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canadian Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div>
-->
					</div>
					<div class="col-sm-4">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
                             <li>
                              <div class="call">
                               <div class="call-img">
                                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                                    <symbol id="call" viewBox="0 0 850 850">
                                        <title>call</title>
                                        <path d="M499.639,396.039l-103.646-69.12c-13.153-8.701-30.784-5.838-40.508,6.579l-30.191,38.818
                                               c-3.88,5.116-10.933,6.6-16.546,3.482l-5.743-3.166c-19.038-10.377-42.726-23.296-90.453-71.04s-60.672-71.45-71.049-90.453
                                               l-3.149-5.743c-3.161-5.612-1.705-12.695,3.413-16.606l38.792-30.182c12.412-9.725,15.279-27.351,6.588-40.508l-69.12-103.646
                                               C109.12,1.056,91.25-2.966,77.461,5.323L34.12,31.358C20.502,39.364,10.511,52.33,6.242,67.539
                                               c-15.607,56.866-3.866,155.008,140.706,299.597c115.004,114.995,200.619,145.92,259.465,145.92
                                               c13.543,0.058,27.033-1.704,40.107-5.239c15.212-4.264,28.18-14.256,36.181-27.878l26.061-43.315
                                               C517.063,422.832,513.043,404.951,499.639,396.039z M494.058,427.868l-26.001,43.341c-5.745,9.832-15.072,17.061-26.027,20.173
                                               c-52.497,14.413-144.213,2.475-283.008-136.32S8.29,124.559,22.703,72.054c3.116-10.968,10.354-20.307,20.198-26.061
                                               l43.341-26.001c5.983-3.6,13.739-1.855,17.604,3.959l37.547,56.371l31.514,47.266c3.774,5.707,2.534,13.356-2.85,17.579
                                               l-38.801,30.182c-11.808,9.029-15.18,25.366-7.91,38.332l3.081,5.598c10.906,20.002,24.465,44.885,73.967,94.379
                                               c49.502,49.493,74.377,63.053,94.37,73.958l5.606,3.089c12.965,7.269,29.303,3.898,38.332-7.91l30.182-38.801
                                               c4.224-5.381,11.87-6.62,17.579-2.85l103.637,69.12C495.918,414.126,497.663,421.886,494.058,427.868z"></path>
                                        <path d="M291.161,86.39c80.081,0.089,144.977,64.986,145.067,145.067c0,4.713,3.82,8.533,8.533,8.533s8.533-3.82,8.533-8.533
                                               c-0.099-89.503-72.63-162.035-162.133-162.133c-4.713,0-8.533,3.82-8.533,8.533S286.448,86.39,291.161,86.39z"></path>
                                        <path d="M291.161,137.59c51.816,0.061,93.806,42.051,93.867,93.867c0,4.713,3.821,8.533,8.533,8.533
                                               c4.713,0,8.533-3.82,8.533-8.533c-0.071-61.238-49.696-110.863-110.933-110.933c-4.713,0-8.533,3.82-8.533,8.533
                                               S286.448,137.59,291.161,137.59z"></path>
                                        <path d="M291.161,188.79c23.552,0.028,42.638,19.114,42.667,42.667c0,4.713,3.821,8.533,8.533,8.533s8.533-3.82,8.533-8.533
                                                            c-0.038-32.974-26.759-59.696-59.733-59.733c-4.713,0-8.533,3.82-8.533,8.533S286.448,188.79,291.161,188.79z"></path>
                                    </symbol>
                                </svg>
                                <svg class="icon" viewBox="0 0 40 40">
                                    <use xlink:href="#call" x="19%" y="18%"></use>
                                </svg>
                            </div>
                            <div class="call-text">
                                <div class="call-title">Contact Us</div>
                                <span>
                                    <a class="call-num" href="/pages/contact">+91 123456789</a>
                                </span>
                            </div>
                              </div>
                             </li>
                             <li>
                              <div class="call">
                               <div class="call-img">
                                <span class="cart-logo hidden-lg-down">
	<svg
		xmlns="http://www.w3.org/2000/svg" style="display: none;">
		<symbol id="shopping-cart" viewBox="0 0 630 630">
			<title>shopping-cart</title>
			<path d="m450.026 192.65h-31l-87.436-126.828a7 7 0 1 0 -11.526 7.945l81.955 118.883h-286.083l81.954-118.883a7 7 0 1 0 -11.526-7.945l-87.432 126.828h-36.958a29.492 29.492 0 1 0 0 58.983h5.226l17.591 173.3a26.924 26.924 0 0 0 26.862 24.273h288.691a26.922 26.922 0 0 0 26.861-24.273l17.592-173.3h5.229a29.492 29.492 0 1 0 0-58.983zm-36.749 230.868a12.962 12.962 0 0 1 -12.933 11.687h-288.688a12.962 12.962 0 0 1 -12.933-11.687l-17.448-171.885h349.45zm36.749-185.885h-388.052a15.492 15.492 0 1 1 0-30.983h388.052a15.492 15.492 0 1 1 0 30.983z"></path>
			<path d="m256 407.526a7 7 0 0 0 7-7v-115.296a7 7 0 0 0 -14 0v115.3a7 7 0 0 0 7 6.996z"></path>
			<path d="m335.57 407.526a7 7 0 0 0 7-7v-115.296a7 7 0 0 0 -14 0v115.3a7 7 0 0 0 7 6.996z"></path>
			<path d="m176.43 407.526a7 7 0 0 0 7-7v-115.296a7 7 0 0 0 -14 0v115.3a7 7 0 0 0 7 6.996z"></path>
		</symbol>
	</svg>
	<svg class="icon" viewBox="0 0 40 40">
		<use xlink:href="#shopping-cart" x="8%" y="7%"></use>
	</svg>
</span>
                            </div>
                            <div class="call-text">
                                <div class="call-title">Cart Item</div>
                                <span>
                                    <a class="call-num" href="/pages/contact"> ({{ $cartCount }}) INR 0.00</a>
                                </span>
                            </div>
                              </div>
                             </li>
<!--								<li><a href="{{ url('/orders') }}"><i class="fa fa-crosshairs"></i> Orders</a></li>-->
<!--
								<li><a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart"></i> Cart ({{ $cartCount }})</a></li>
								@if(empty(Auth::check()))
									<li><a href="{{ url('/login-register') }}"><i class="fa fa-lock"></i> Login</a></li>
								@else
									<li><a href="{{ url('/account') }}"><i class="fa fa-user"></i> Account</a></li>
									<li><a href="{{ url('/user-logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
								@endif
-->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="{{ url('/') }}" class="active">Home</a></li>
								<li class="dropdown">                                
                                <a id="dLabel" role="button" data-toggle="dropdown" class="btn btn-primary" data-target="#">SHOP BY CATEGORY <i class="fa fa-angle-down"></i></a>
                                 <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
								 @foreach($mainCategories as $cat)
								 <li  class="dropdown-submenu"><a href="{{ asset('products/'.$cat->url) }}">{{ $cat->name }}</a>
                                   <ul class="dropdown-menu">
								   @foreach($cat->categories as $subcat)
								   
								   <?php $productCount = Product::productCount($subcat->id); ?>
                                    <!--<li><a tabindex="-1" href="#">Second level</a></li>-->
									@if($subcat->status==1)
									<li><a tabindex="-1" href="{{ asset('products/'.$subcat->url) }}">{{$subcat->name}} ({{ $productCount }})</a> </li>
									@endif					
																	
									@endforeach									
                                   </ul>								   
                                  </li>
								  @endforeach							
                                 </ul>
                                </li> 
<!--
								<li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="blog.html">Blog List</a></li>
										<li><a href="blog-single.html">Blog Single</a></li>
                                    </ul>
                                </li> 
-->
								<li><a href="#">About Us</a></li>
								<li><a href="{{ url('page/post') }}">Contact</a></li>
								<li><a href="#">Faq's</a></li>
							</ul>
						</div>						
					</div>
					<div class="col-sm-3">
<!--
						<div class="search_box pull-right">
							<form action="{{ url('/search-products') }}" method="post">{{ csrf_field() }} 
								<input type="text" placeholder="Search Product" name="product" />
								<button type="submit" style="border:0px; height:35px; margin-left:-3px">Go</button>
							</form>
						</div>
-->
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->