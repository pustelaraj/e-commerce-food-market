<?php use App\Product; ?>
<form action="{{ url('/products-filter') }}" method="post">{{ csrf_field() }}
	@if(!empty($url))
	<input name="url" value="{{ $url }}" type="hidden">
	@endif 
	<div class="left-sidebar">
		<h2>Category</h2>
		<div class="panel-group category-products" id="accordian">
			@foreach($categories as $cat)
         <div class="panel panel-default">
         <div class="panel-heading">
          <h4 class="panel-title">
           <a data-toggle="collapse" data-parent="#accordian" href="#{{$cat->id}}">
               <span class="badge pull-right"><i class="fa fa-plus"></i></span>
               {{$cat->name}}
           </a>
          </h4>
         </div>
         <div id="{{$cat->id}}" class="panel-collapse collapse">
          <div class="panel-body">
           <ul>
             @foreach($cat->categories as $subcat)
                 <?php $productCount = Product::productCount($subcat->id); ?>

                 @if($subcat->status==1)
                 <li><a href="{{ asset('products/'.$subcat->url) }}">{{$subcat->name}} </a> ({{ $productCount }})</li>
                 @endif
                 @if ($subcat->categories)
                 <li>
                 @foreach ($subcat->categories as $childCategory)									
                 <li style="padding-left:10px;"><a href="{{ asset('products/'.$childCategory->url) }}">{{$childCategory->name}} </a> </li>
                 @endforeach
                 </li>
                 @endif
                 @endforeach						

           </ul>
          </div>
         </div>
				</div>
			@endforeach
		</div>

		@if(!empty($url))
		
			<h2>Brands</h2>	
			
			<div class="panel-group">
				@foreach($brandArray as $brand)
					@if(!empty($_GET['brand']))
						<?php $brandArr = explode('-',$_GET['brand']) ?>
						@if(in_array($brand,$brandArr))
							<?php $brandcheck="checked"; ?>	
						@else
							<?php $brandcheck=""; ?>
						@endif		
					@else
						<?php $brandcheck=""; ?>
					@endif
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<input name="barandFilter[]" onchange="javascript:this.form.submit();" id="{{ $brand }}" value="{{ $brand }}" type="checkbox" {{ $brandcheck }}>&nbsp;&nbsp;<span class="products-colors">{{ $brand }}</span>
							</h4>
						</div>
					</div>
				@endforeach
			</div>
			

			<div>&nbsp;</div>
			
			<h2>Food Preference</h2>	
			<div class="panel-group">
				@foreach($foodpreference as $fps)
					@if(!empty($_GET['fp']))
						<?php $fpArr = explode('-',$_GET['fp']) ?>
						@if(in_array($fps,$fpArr))
							<?php $fpcheck="checked"; ?>	
						@else
							<?php $fpcheck=""; ?>
						@endif		
					@else
						<?php $fpcheck=""; ?>
					@endif
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<input name="fpFilter[]" onchange="javascript:this.form.submit();" id="{{ $fps }}" value="{{ $fps }}" type="checkbox" {{ $fpcheck }}>&nbsp;&nbsp;<span class="products-sleeves">{{ $fps }}</span>
							</h4>
						</div>
					</div>
				@endforeach
			</div>
			

			<div>&nbsp;</div>
			<!--
			<h2>Pattern</h2>	
			<div class="panel-group">
				@foreach($patternArray as $pattern)
					@if(!empty($_GET['pattern']))
						<?php $patternArr = explode('-',$_GET['pattern']) ?>
						@if(in_array($pattern,$patternArr))
							<?php $patterncheck="checked"; ?>	
						@else
							<?php $patterncheck=""; ?>
						@endif		
					@else
						<?php $patterncheck=""; ?>
					@endif
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<input name="patternFilter[]" onchange="javascript:this.form.submit();" id="{{ $pattern }}" value="{{ $pattern }}" type="checkbox" {{ $patterncheck }}>&nbsp;&nbsp;<span class="products-patterns">{{ $pattern }}</span>
							</h4>
						</div>
					</div>
				@endforeach
			</div>
		-->
			<div>&nbsp;</div>
			
			<h2>Pack Size</h2>	
			<div class="panel-group">
				@foreach($sizesArray as $size)
					@if(!empty($_GET['size']))
						<?php $sizeArr = explode('-',$_GET['size']) ?>
						@if(in_array($size,$sizeArr))
							<?php $sizecheck="checked"; ?>	
						@else
							<?php $sizecheck=""; ?>
						@endif		
					@else
						<?php $sizecheck=""; ?>
					@endif
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<input name="sizeFilter[]" onchange="javascript:this.form.submit();" id="{{ $size }}" value="{{ $size }}" type="checkbox" {{ $sizecheck }}>&nbsp;&nbsp;<span class="products-sizes">{{ $size }}</span>
							</h4>
						</div>
					</div>
				@endforeach
			</div>
			

			<div>&nbsp;</div>

		@endif
		
	</div>
</form>