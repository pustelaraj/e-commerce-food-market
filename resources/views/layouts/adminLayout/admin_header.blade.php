<!--Header-part-->
<div id="header">
 <a href="#"><img src="{{ asset('images/backend_images/logo.png') }}" alt="" /></a>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav"><li class=""><a title="" href="javascript:void(0)"><span class="text">Welcome {{ Session::get('adminDetails')['username'] }} ({{ Session::get('adminDetails')['type'] }})</span></a></li>
    <li class=""><a title="" href="{{ url('/admin/settings') }}"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
    <li class=""><a title="" href="{{ url('/logout') }}"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
 <div class="input-group">
   <input type="text" class="form-control" placeholder="Search...">
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
  </div>
<!--
 
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
-->
</div>
<!--close-top-serch-->