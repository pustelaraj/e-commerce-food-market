<?php 
use App\Http\Controllers\Controller;
use App\Product;
$mainCategories =  Controller::mainCategories();
$cartCount = Product::cartCount();
?>
@extends('layouts.frontLayout.front_design')
@section('content')

<section id="slider">
<!-- slider-->
	<div class="container-fluid">
<!--		<div class="row">-->
<!--			<div class="col-sm-12">-->
				<div id="slider-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						@foreach($banners as $key => $banner)
							<li data-target="#slider-carousel" data-slide-to="0" @if($key==0) class="active" @endif></li>
						@endforeach
					</ol>					
					<div class="carousel-inner">
						@foreach($banners as $key => $banner)
						<div class="item @if($key==0) active @endif">
							<a href="{{ $banner->link }}" title="Banner 1"><img src="images/frontend_images/banners/{{ $banner->image }}"></a>
						</div>
						@endforeach
					</div>					
					<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>				
			</div>
<!--		</div>-->
<!--	</div>-->
</section>
<!--/slider-->
<!--shop by Category-->
<section id="shopByCat">
 <div class="container-fluid">
<!--
  <div class="head">
   <h3>Shop By Department <span>Choose What You Looking For</span></h3>
  </div>
-->
  <div class="row">
  @foreach($mainCategories as $cat)
   <div class="bannerblock1 bannerblock col-sm-3 col-xs-6">
	<div class="image-container">
		<a class="ishi-customhover-fadeinrotate " href="{{ asset('products/'.$cat->url) }}">
			<img src="{{ asset('images/backend_images/category/medium') }}/{{$cat->image}}" data-src="" data-widths="[375]" data-sizes="auto" title="{{$cat->name}}" alt="{{$cat->name}}">
    </a>
		</div>
	</div>
  @endforeach  
  </div>
 </div>
</section>
<!--/shop by Category-->

<!--/deal of the day-->
<section id="ourProducts">
	<div class="container">
     <div class="section-header text-center">
	<div class="seperator" style="background-image: url('https://cdn.shopify.com/s/files/1/0086/0251/7559/files/title.png?v=1577861652');"></div>
	<h2 class="home-title">Our Products</h2>
     </div>
     <div class="row">
      <div class="col-sm-12 padding-right">
       <div class="features_items"><!--features_items-->
        @foreach($productsAll as $pro)
        <div class="col-md-3 col-sm-6">
         <div class="product-grid2">
          <div class="product-image2">
           <a href="{{ url('/product/'.$pro->id) }}">
            <img class="pic-1" src="{{ asset('/images/backend_images/product/small/'.$pro->image) }}">
<!--            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo3/images/img-2.jpeg">-->
           </a>
           <ul class="social">
<!--            <li><a href="{{ url('/product/'.$pro->id) }}" data-tip="Quick View"><i class="fa fa-eye"></i></a></li>-->
            <li><a href="#" data-tip="Add to Wishlist"><i class="fa fa-star"></i></a></li>
            <li><a href="{{ url('/product/'.$pro->id) }}" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
           </ul>
           <a class="add-to-cart" href="{{ url('/product/'.$pro->id) }}">Add to cart</a>
          </div>
          <div class="product-content">
           <h3 class="title"><a href="{{ url('/product/'.$pro->id) }}">{{ $pro->product_name }}</a></h3>
           <div class="cost">INR <span class="price">{{ $pro->price }}</span> <span class="price"><del>Rs{{ $pro->price }}</del></span></div>
          </div>
         </div>
        </div>
        @endforeach
       </div><!--features_items-->
<!--					<div align="center">{{ $productsAll->links() }}</div>-->
			</div>
		</div>
	</div>
</section>
<!--deal of the day-->

<section id="dealOfTheDay">
 <div class="container-fluid">
  <div class="head">
   <h3>Deal Of The Day
    <span>Choose What You Looking For</span>
   </h3>
  </div>`
  <div class="row">   
<div class="col-sm-8">
	<div id="dealOftheDayProduct" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
		<div class="carousel-inner">





    @foreach($dealsproducts as $key => $deals)
						<div class="item @if($key==0) active @endif">	
		
				<div class="row">
					<div class="col-sm-12">
						<div class="product-grid7">
							<div class="product-image7">
							
                <a href="{{ url('/product/'.$deals->id) }}" title="Banner 1">
                  <img class="pic-1" src="images/backend_images/product/medium/{{ $deals->image }}">
                  <img class="pic-2" src="images/backend_images/product/medium/{{ $deals->image }}">                
                </a>
                		<!--<img class="pic-1" src="https://images-na.ssl-images-amazon.com/images/I/51jBGmzSeCL.jpg">
										<img class="pic-2" src="https://images-na.ssl-images-amazon.com/images/I/51jBGmzSeCL.jpg">-->
										</a>
										<ul class="social">
											<li><a href="" class="fa fa-search"></a></li>
											<li><a href="" class="fa fa-shopping-bag"></a></li>
											<li><a href="" class="fa fa-shopping-cart"></a>
											</li>
										</ul>
									</div>
									<div class="product-content">
										<h3 class="title">
											<a href="#"> {{$deals->name}}  </a>
										</h3>
										<ul class="rating">
											<li class="fa fa-star"></li>
											<li class="fa fa-star"></li>
											<li class="fa fa-star"></li>
											<li class="fa fa-star"></li>
											<li class="fa fa-star"></li>
										</ul>
										<div class="price">INR{{$deals->price}}
											<span>Rs{{$deals->actual_price}}</span>
										</div>
										<p>{{$deals->description}}</p>
										<div id="dealCount{{$deals->deals_expiry_date}}"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
          @endforeach				
     </div>
      <div class="controls">
       <a class="left fa fa-chevron-left btn btn-primary" href="#dealOftheDayProduct" data-slide="prev"></a>
       <a class="right fa fa-chevron-right btn btn-primary" href="#dealOftheDayProduct" data-slide="next"></a>
     </div>
 </div>
   </div>   
<div class="col-sm-4">
  <div class="specialProduct">
  <div class="heading">
   <div class="row">
    <div class="col-md-9"><h3>Combo Product</h3></div>
    <div class="col-md-3">
     <!-- Controls -->
     <div class="controls pull-right hidden-xs">
      <a class="left fa fa-chevron-left btn btn-primary" href="#specialProduct" data-slide="prev"></a>
      <a class="right fa fa-chevron-right btn btn-primary" href="#specialProduct" data-slide="next"></a>
     </div>
    </div>
   </div>
  </div>
  <div id="specialProduct" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
   <div class="carousel-inner">
   @foreach($comboproducts as $key => $combo)
						<div class="item @if($key==0) active @endif">	
     <div class="row">
      <div class="col-sm-12">
       <div class="product-grid7">
        <div class="product-image7">
        <a href="{{ url('/product/'.$combo->id) }}" title="Banner 1">
                  <img class="pic-1" src="images/backend_images/product/medium/{{ $combo->image }}">
                  <img class="pic-2" src="images/backend_images/product/medium/{{ $combo->image }}">                
                </a>
        </div>
        <div class="product-content">
         <h5 class="title"><a href="#">{{$combo->name}}</a> </h5>
         <ul class="rating">
             <li class="fa fa-star"></li>
             <li class="fa fa-star"></li>
             <li class="fa fa-star"></li>
             <li class="fa fa-star"></li>
             <li class="fa fa-star"></li>
         </ul>
         <div class="price">{{$combo->price}}  <span>Rs{{$combo->actual_price}}</span> </div>
        </div>
       </div>
       @endforeach      
       
       </div>
      </div>
     </div>
    </div>
    
      
       
       </div>
      </div>
     </div>
    </div>
     </div>
  </div>
  </div>
   </div>
   </div>
   </div>
</section>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function() {
//var substr =  [184,185];
  var expirydates = <?php echo $dealofdates;?>;
  $.each(expirydates , function(index,val) { 
    console.log(val)
      var countDownDate = new Date(val).getTime();
    // Update the count down every 1 second
      var x = setInterval(function() {
      // Get today's date and time
      var now = new Date().getTime();    
      // Find the distance between now and the count down date
      var distance = countDownDate - now;    
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);        
      // Output the result in an element with id="demo"
      document.getElementById('dealCount'+val).innerHTML ="<ul>" + "<li>" + days + "<span>Days</span></li>" + "<li>" + hours + "<span> Hrs</span></li>" +
      "<li>" + minutes + "<span> Mins</span></li>" + "<li>" + seconds + "<span> Sec</span></li>" + "</ul>";
        // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById('dealCount'+val).innerHTML = "EXPIRED";
      }
    }, 1000);
  });
});
</script>
<!--/deal of the day-->





@endsection